import { Component } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AboutPage } from '../about/about';
import { AddProductPage } from '../add-product/add-product';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  searchQuery: string = '';
  items: string[];

  constructor(public navCtrl: NavController,
    private barcodeScanner: BarcodeScanner,
    private alertCtrl: AlertController, private toastCtrl: ToastController) {
      this.initializeItems()
  }

  about(){
    this.navCtrl.push(AboutPage);
  }

  scan(){
    this.barcodeScanner.scan()
    .then(barcodeData => {
      this.alertCtrl.create({
        title: 'Info',
        subTitle: JSON.stringify(barcodeData), //text
        buttons: ['Dismiss']
        }).present();
     })
    .catch(err => {
        this.alertCtrl.create({
        title: 'Info',
        subTitle: err,
        buttons: ['Dismiss']
        }).present();
    })
  }

  initializeItems() {
    this.items = [
      'Amsterdam',
      'Bogota',
    ];
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  addItem(){
    // this.navCtrl.push(AddProductPage);
    const alert = this.alertCtrl.create({
      title: 'Add Your Product',
      inputs: [
        {
          name: 'listProduct',
          placeholder: 'Product Name'
        },
        {
          name: 'categoryProduct',
          placeholder: 'Category'
        },
        {
          name: 'priceProduct',
          placeholder: 'Price'
        }
      ],
      buttons: [
        {
          text: 'OK',
          // handler: (data: any) => {
          //   this.productService.addYourProduct({'id':String(this.productService.getProductID()), 'item':data.listProduct, 'category':data.categoryProduct});
          //   // this.add-product = this.productService.getProductList();
          //   let toast = this.toastCtrl.create({
          //     message: "New Product Succesfull added",
          //     duration: 1000,
          //     position: 'bottom'
          //   });
          //   toast.present();
          //   console.log(this.productService);
          // }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });

    alert.present();
  }
}
