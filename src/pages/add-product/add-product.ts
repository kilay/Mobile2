import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Item, List } from 'ionic-angular';
import { ProductListservice } from "../../services/product-list/product-list.service";


/**
 * Generated class for the AddProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-product',
  templateUrl: 'add-product.html',
})
export class AddProductPage {
    Name: '';
    Quantity: undefined;
    Price: undefined;

  constructor(public navCtrl: NavController, public navParams: NavParams, public product: ProductListservice) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddProductPage');
  }

  // addItem(item: AddProductPage) {
  //   this.product.addItem(item).then(ref => {
  //     this.navCtrl.setRoot('HomePage', {key: ref.key})
  //   });
  // }

}
