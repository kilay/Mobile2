import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { UserAuthenticationService } from '../../services/UserAuthentication.service';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  pageTitle: string 
  userAuth: FormGroup

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder,
    private authSvc: UserAuthenticationService) {
    this.pageTitle = this.navParams.get('title')
    this.userAuth = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      pwd: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

login(userAuth: NgForm){
    this.authSvc.signin(userAuth.value.email, userAuth.value.pwd)
    .then((auth : any) =>
    {
      this.navCtrl.setRoot(HomePage);
    })
    .catch((error : any) =>
    {
      console.log(error.message);
    });
  }

  noAccount(){
    this.navCtrl.setRoot(RegisterPage);
  }
}
