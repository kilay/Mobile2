import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { UserAuthenticationService } from '../../services/UserAuthentication.service';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  userAuth: FormGroup

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder, private authSvc: UserAuthenticationService) {
    this.userAuth = this.formBuilder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      pwd: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  ionViewDidLoad() {
    
  }

  register(userAuth: NgForm){
    this.authSvc.signup(userAuth.value.email, userAuth.value.pwd);
    this.navCtrl.setRoot(LoginPage);
  }

}
