import { Injectable } from "@angular/core";

// import { Product } from "ionic-angular";
import { AddProductPage } from "../../pages/add-product/add-product";

@Injectable()
export class ProductListservice{

    private productListRef = 'product-list';

    constructor(){

    }

    getProductList() {
        return this.productListRef;
    }

    // addItem(item: AddProductPage) {
    //     return this.productListRef.push(item);
    // }
} 